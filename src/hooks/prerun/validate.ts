import { Hook } from "@oclif/config";
import { configparam, configstore } from "../../configstore";
import { messages } from "../../errors";

const isProjectSet = () => {
  if (!configstore.has(configparam.PROJECT)) {
    throw new Error(messages.PROJECT_NOT_SET);
  }

  return true;
};

const isSceneSet = () => {
  if (!configstore.has(configparam.SCENE)) {
    throw new Error(messages.SCENE_NOT_SET);
  }

  return true;
};

const isTeamSet = () => {
  if (!configstore.has(configparam.TEAM)) {
    throw new Error(messages.TEAM_NOT_SET);
  }

  return true;
};

const hook: Hook<"prerun"> = async function (opts) {
  const validations = {
    CharacterCreate: [isTeamSet, isProjectSet, isSceneSet],
    CharacterLink: [isTeamSet, isProjectSet, isSceneSet],
    LocationCreate: [isTeamSet, isProjectSet],
    Project: [isTeamSet, isProjectSet],
    ProjectCharacters: [isTeamSet, isProjectSet],
    ProjectCreate: [isTeamSet],
    ProjectList: [isTeamSet],
    ProjectLocations: [isTeamSet, isProjectSet],
    ProjectSet: [isTeamSet],
    Scene: [isTeamSet, isProjectSet, isSceneSet],
    SceneCharacters: [isTeamSet, isProjectSet, isSceneSet],
    SceneCreate: [isTeamSet, isProjectSet],
    SceneList: [isTeamSet, isProjectSet],
    SceneSet: [isTeamSet, isProjectSet],
    SceneShots: [isTeamSet, isProjectSet, isSceneSet],
    Team: [isTeamSet],
  };

  try {
    Object.entries(validations).forEach(([k, v]) => {
      if (k === opts.Command.name) {
        v.forEach((f) => {
          f();
        });
      }
    });
  } catch (error) {
    this.error(error.message);
  }
};

export default hook;
