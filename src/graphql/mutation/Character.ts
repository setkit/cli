import gql from "graphql-tag";

const createCharacter = gql`
  mutation CreateCharacter($name: String!, $projectId: ID!) {
    createCharacter(name: $name, projectId: $projectId) {
      id
      name
      number
    }
  }
`;

const linkCharacterToScene = gql`
  mutation LinkCharacterToScene($characterId: ID!, $sceneId: ID!) {
    linkCharacterToScene(characterId: $characterId, sceneId: $sceneId) {
      id
      name
      number
    }
  }
`;

export default {
  createCharacter,
  linkCharacterToScene,
};
