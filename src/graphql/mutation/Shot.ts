import gql from "graphql-tag";

const createShot = gql`
  mutation CreateShot(
    $angle: String!
    $description: String
    $grip: String!
    $movment: String!
    $sceneId: ID!
    $type: String!
  ) {
    createShot(
      angle: $angle
      description: $description
      grip: $grip
      movment: $movment
      sceneId: $sceneId
      type: $type
    ) {
      angle
      description
      grip
      id
      movment
      scene
      type
    }
  }
`;

export default { createShot };
