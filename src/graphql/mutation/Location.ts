import gql from "graphql-tag";

const createLocation = gql`
  mutation CreateLocation($name: String!, $projectId: ID!) {
    createLocation(name: $name, projectId: $projectId) {
      id
      name
    }
  }
`;

export default {
  createLocation,
};
