import gql from "graphql-tag";

const createProject = gql`
  mutation CreateProject($title: String!, $teamId: ID!) {
    createProject(title: $title, teamId: $teamId) {
      id
      title
      visibility
      isArchived
      team {
        name
      }
    }
  }
`;
export default {
  createProject,
};
