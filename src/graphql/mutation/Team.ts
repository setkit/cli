import gql from "graphql-tag";

const createTeam = gql`
  mutation CreateTeam($name: String!) {
    createTeam(name: $name) {
      id
      name
    }
  }
`;

export default {
  createTeam,
};
