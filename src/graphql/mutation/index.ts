import Character from "./Character";
import Location from "./Location";
import Project from "./Project";
import Scene from "./Scene";
import Shot from "./Shot";
import Team from "./Team";

export default {
  ...Character,
  ...Location,
  ...Project,
  ...Scene,
  ...Shot,
  ...Team,
};
