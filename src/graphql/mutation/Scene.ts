import gql from "graphql-tag";

const createScene = gql`
  mutation CreateScene($projectId: ID!, $slugline: String!) {
    createScene(projectId: $projectId, slugline: $slugline) {
      id
      slugline
    }
  }
`;

export default {
  createScene,
};
