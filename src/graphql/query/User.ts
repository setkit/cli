import gql from "graphql-tag";

const teamMembers = gql`
  query TeamMembers($teamId: ID!) {
    teamMembers(teamId: $teamId) {
      id
    }
  }
`;

export default {
  teamMembers,
};
