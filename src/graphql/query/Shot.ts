import gql from "graphql-tag";

const shotsByCharacter = gql`
  query ShotsByScene($characterId: ID!) {
    shotsByScene(characterId: $characterId) {
      angle
      characters {
        id
      }
      complete
      description
      grip
      id
      movement
      order
      type
    }
  }
`;

const shotsByScene = gql`
  query ShotsByScene($sceneId: ID!) {
    shotsByScene(sceneId: $sceneId) {
      angle
      characters {
        id
      }
      complete
      description
      grip
      id
      movement
      order
      type
    }
  }
`;

export default {
  shotsByCharacter,
  shotsByScene,
};
