import gql from "graphql-tag";

const locationsByProject = gql`
  query LocationsByProject($projectId: ID!) {
    locationsByProject(projectId: $projectId) {
      id
      name
    }
  }
`;

const locationsByScene = gql`
  query LocationsByProject($sceneId: ID!) {
    locationsByProject(sceneId: $sceneId) {
      id
      name
    }
  }
`;

export default {
  locationsByProject,
  locationsByScene,
};
