import gql from "graphql-tag";

const myTeams = gql`
  query MyTeams {
    myTeams {
      id
      name
    }
  }
`;

export default { myTeams };
