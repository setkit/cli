import gql from "graphql-tag";

const projectsByTeam = gql`
  query ProjectsByTeam($teamId: ID!) {
    projectsByTeam(teamId: $teamId) {
      id
      title
      visibility
      isArchived
      characters {
        id
        name
      }
    }
  }
`;

export default {
  projectsByTeam,
};
