import gql from "graphql-tag";

const charactersByProject = gql`
  query CharactersByProject($projectId: ID!) {
    charactersByProject(projectId: $projectId) {
      id
      name
      number
    }
  }
`;

const charactersByScene = gql`
  query CharactersByScene($sceneId: ID!) {
    charactersByScene(sceneId: $sceneId) {
      id
      name
      number
    }
  }
`;

const charactersByShot = gql`
  query CharactersByScene($shotId: ID!) {
    charactersByScene(shotId: $shotId) {
      id
      name
      number
    }
  }
`;

export default {
  charactersByProject,
  charactersByScene,
  charactersByShot,
};
