import gql from "graphql-tag";

const scenesByCharacter = gql`
  query ScenesByProject($characterId: ID!) {
    scenesByProject(characterId: $characterId) {
      id
      slugline
    }
  }
`;

const scenesByLocation = gql`
  query ScenesByProject($locationId: ID!) {
    scenesByProject(locationId: $locationId) {
      id
      slugline
    }
  }
`;

const scenesByProject = gql`
  query ScenesByProject($projectId: ID!) {
    scenesByProject(projectId: $projectId) {
      id
      slugline
    }
  }
`;

export default {
  scenesByCharacter,
  scenesByLocation,
  scenesByProject,
};
