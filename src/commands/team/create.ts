import { Command, flags } from "@oclif/command";
import chalk from "chalk";
import * as inquirer from "inquirer";
import api from "../../api";
import graphql from "../../graphql";
import { configparam, configstore } from "../../configstore";

export default class TeamCreate extends Command {
  static description = "Create a new team.";

  static aliases = ["tm:cr"];

  static flags = {
    help: flags.help({ char: "h" }),
    name: flags.string({ char: "n", description: "Name of new team." }),
  };

  static args = [];

  async run() {
    const { flags } = this.parse(TeamCreate);

    let teamName;
    if (flags.name) {
      teamName = flags.name;
    } else {
      const iq = await inquirer.prompt([
        {
          type: "input",
          name: "teamName",
          message: "Team name:",
        },
      ]);
      teamName = iq.teamName;
    }

    let result = await api(graphql.mutation.createTeam, { name: teamName });
    const { id, name } = result?.data.data.createTeam;

    console.log(
      `Team ${chalk.underline.cyan(name)} created ${chalk.green(
        "successfully"
      )}!`
    );

    configstore.delete(configparam.TEAM);
    configstore.set({ team: { id, name } });
  }
}
