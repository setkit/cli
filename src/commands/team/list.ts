import { Command, flags } from "@oclif/command";
import { cli } from "cli-ux";
import api from "../../api";
import graphql from "../../graphql";

export default class TeamList extends Command {
  static description = "Display the current user's team names.";

  static aliases = ["tm:ls"];

  static flags = {
    help: flags.help({ char: "h" }),
  };

  static args = [];

  async run() {
    let result = await api(graphql.query.myTeams);
    const teams = result.data.data.myTeams;

    if (!teams.length) {
      this.error("No teams exist for this user.");
    }

    cli.table(
      teams,
      {
        id: {
          header: "ID",
          extended: true,
        },
        name: {
          minWidth: 7,
        },
      },
      {
        printLine: this.log,
        ...flags, // parsed flags
      }
    );
  }
}
