import { Command, flags } from "@oclif/command";
import * as inquirer from "inquirer";
import api from "../../api";
import graphql from "../../graphql";
import { configparam, configstore } from "../../configstore";

export default class TeamSet extends Command {
  static description = "Set the active team.";

  static aliases = ["tm:st"];

  static flags = {
    help: flags.help({ char: "h" }),
  };

  static args = [];

  async run() {
    let result = await api(graphql.query.myTeams);
    const teams = result.data.data.myTeams;

    if (!teams.length) {
      this.error("No teams exist for this user.");
    }

    const def = teams.find(
      (t: any) => t.id === configstore.get(configparam.TEAM_ID)
    );

    const answers = await inquirer.prompt([
      {
        type: "list",
        name: "teamId",
        message: "Select team:",
        default: def?.id,
        choices: teams.map((t: any) => {
          return {
            name: t.name,
            value: t.id,
          };
        }),
      },
    ]);

    const { id, name } = teams.find((t: any) => t.id === answers.teamId);

    configstore.delete(configparam.TEAM);
    configstore.set({ [configparam.TEAM]: { id, name } });
  }
}
