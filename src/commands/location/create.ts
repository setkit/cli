import { Command, flags } from "@oclif/command";
import chalk from "chalk";
import * as inquirer from "inquirer";
import api from "../../api";
import graphql from "../../graphql";
import { configstore, configparam } from "../../configstore";

export default class LocationCreate extends Command {
  static description = "describe the command here";

  static aliases = ["lc:cr"];

  static flags = {
    help: flags.help({ char: "h" }),
    // flag with a value (-n, --name=VALUE)
    name: flags.string({ char: "n", description: "Name of new location." }),
  };

  static args = [{ name: "file" }];

  async run() {
    const { flags } = this.parse(LocationCreate);

    let locationName;
    if (flags.name) {
      locationName = flags.name;
    } else {
      const iq = await inquirer.prompt([
        {
          type: "input",
          name: "locationName",
          message: "Location name:",
        },
      ]);
      locationName = iq.locationName;
    }

    let result = await api(graphql.mutation.createLocation, {
      name: locationName,
      projectId: configstore.get(configparam.PROJECT_ID),
    });
    const { name } = result.data.data.createLocation;

    console.log(
      `Character ${chalk.underline.cyan(name)} created ${chalk.green(
        "successfully"
      )}!`
    );
  }
}
