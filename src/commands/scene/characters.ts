import { Command, flags } from "@oclif/command";
import { cli } from "cli-ux";
import api from "../../api";
import graphql from "../../graphql";
import { configparam, configstore } from "../../configstore";

export default class SceneCharacters extends Command {
  static description = "describe the command here";

  static aliases = ["sc:ch"];

  static flags = {
    help: flags.help({ char: "h" }),
  };

  static args = [];

  async run() {
    let result = await api(graphql.query.charactersByScene, {
      sceneId: configstore.get(configparam.SCENE_ID),
    });
    const characters = result.data.data.charactersByScene;

    cli.table(
      characters,
      {
        id: {
          header: "ID",
          extended: true,
        },
        number: {
          header: "#",
          minWidth: 3,
        },
        name: {
          minWidth: 7,
        },
      },
      {
        printLine: this.log,
        ...flags, // parsed flags
      }
    );
  }
}
