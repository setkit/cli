import { Command, flags } from "@oclif/command";
import { cli } from "cli-ux";
import api from "../../api";
import graphql from "../../graphql";
import { configparam, configstore } from "../../configstore";

export default class SceneList extends Command {
  static description = "describe the command here";

  static aliases = ["sc:ls"];

  static flags = {
    help: flags.help({ char: "h" }),
  };

  static args = [];

  async run() {
    let result = await api(graphql.query.scenesByProject, {
      projectId: configstore.get(configparam.PROJECT_ID),
    });
    const scenes = result.data.data.scenesByProject;

    if (!scenes.length) {
      this.error("No scenes exist for this project.");
    }

    cli.table(
      scenes,
      {
        id: {
          header: "ID",
          extended: true,
        },
        slugline: {
          minWidth: 7,
        },
      },
      {
        printLine: this.log,
        ...flags, // parsed flags
      }
    );
  }
}
