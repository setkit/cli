import { Command, flags } from "@oclif/command";
import * as inquirer from "inquirer";
import api from "../../api";
import graphql from "../../graphql";
import { configparam, configstore } from "../../configstore";

export default class SceneSet extends Command {
  static description = "describe the command here";

  static aliases = ["sc:st"];

  static flags = {
    help: flags.help({ char: "h" }),
  };

  static args = [];

  async run() {
    let result = await api(graphql.query.scenesByProject, {
      projectId: configstore.get(configparam.PROJECT_ID),
    });
    const scenes = result.data.data.scenesByProject;

    if (!scenes.length) {
      this.error("No scenes exist for this project.");
    }

    const def = scenes.find(
      (s: any) => s.id === configstore.get(configparam.SCENE_ID)
    );

    const iq = await inquirer.prompt([
      {
        type: "list",
        name: "sceneId",
        message: "Select scene:",
        default: def?.id,
        choices: scenes.map((scene: any) => {
          return {
            name: scene.slugline,
            value: scene.id,
          };
        }),
      },
    ]);

    const { id, slugline } = scenes.find((p: any) => p.id === iq.sceneId);

    configstore.delete(configparam.SCENE);
    configstore.set({ [configparam.SCENE]: { id, slugline } });
  }
}
