import { Command, flags } from "@oclif/command";
import { cli } from "cli-ux";
import api from "../../api";
import graphql from "../../graphql";
import { configparam, configstore } from "../../configstore";

export default class SceneShots extends Command {
  static description = "describe the command here";

  static aliases = ["sc:sh"];

  static flags = {
    help: flags.help({ char: "h" }),
  };

  static args = [{ name: "file" }];

  async run() {
    let result = await api(graphql.query.shotsByScene, {
      sceneId: configstore.get(configparam.SCENE_ID),
    });
    const shots = result.data.data.shotsByScene;

    if (!shots.length) {
      this.error("No shots exist for this scene.");
    }

    cli.table(
      shots,
      {
        id: {
          header: "ID",
          extended: true,
        },
        angle: {
          header: "Angle",
          minWidth: 6,
        },
        grip: {
          header: "Grip",
          minWidth: 6,
        },
        movement: {
          header: "Movement",
          minWidth: 6,
        },
        type: {
          header: "Type",
          minWidth: 6,
        },
      },
      {
        printLine: this.log,
        ...flags, // parsed flags
      }
    );
  }
}
