import { Command, flags } from "@oclif/command";
import chalk from "chalk";
import * as inquirer from "inquirer";
import api from "../../api";
import graphql from "../../graphql";
import { configparam, configstore } from "../../configstore";

export default class SceneCreate extends Command {
  static description = "describe the command here";

  static aliases = ["sc:cr"];

  static flags = {
    help: flags.help({ char: "h" }),
    // flag with a value (-n, --name=VALUE)
    setting: flags.string({
      char: "s",
      description: "Setting of new scene.",
    }),
    location: flags.string({
      char: "l",
      description: "Location of new scene",
    }),
    tod: flags.string({
      char: "t",
      description: "Time of day of the scene",
    }),
  };

  static args = [];

  async run() {
    const { flags } = this.parse(SceneCreate);

    let sceneSetting;
    if (flags.setting) {
      sceneSetting = flags.setting.toUpperCase();

      switch (sceneSetting) {
        case "INT.":
        case "EXT.":
        case "INT./EXT.":
          break;
        default:
          console.error("Invalid scene setting.");
          return;
      }
    } else {
      const iq = await inquirer.prompt([
        {
          type: "list",
          name: "sceneSetting",
          message: "Select the setting:",
          choices: [
            {
              name: "INT.",
            },
            {
              name: "EXT.",
            },
            {
              name: "INT./EXT.",
            },
          ],
        },
      ]);
      sceneSetting = iq.sceneSetting;
    }

    let sceneLocation;
    if (flags.location) {
      sceneLocation = flags.location.toUpperCase();
    } else {
      const iq = await inquirer.prompt([
        {
          type: "input",
          name: "sceneLocation",
          message: "What is the scene location?",
        },
      ]);
      sceneLocation = iq.sceneLocation;
    }

    let sceneTOD;
    if (flags.tod) {
      sceneTOD = flags.tod.toUpperCase();

      switch (sceneTOD) {
        case "DAY":
        case "EVENING":
        case "NIGHT":
          break;
        default:
          console.error("Invalid scene time of day.");
          return;
      }

      sceneTOD = flags.tod.toUpperCase();
    } else {
      const iq = await inquirer.prompt([
        {
          type: "list",
          name: "sceneTOD",
          message: "Select the time of day:",
          choices: [
            {
              name: "DAY",
            },
            {
              name: "EVENING",
            },
            {
              name: "NIGHT",
            },
          ],
        },
      ]);
      sceneTOD = iq.sceneTOD;
    }

    let result = await api(graphql.mutation.createScene, {
      projectId: configstore.get(configparam.PROJECT_ID),
      slugline: `${sceneSetting} ${sceneLocation} - ${sceneTOD}`,
    });
    const { id, slugline } = result.data.data.createScene;

    configstore.delete(configparam.SCENE);
    configstore.set({ [configparam.SCENE]: { id, slugline } });
    console.log(
      `Scene ${chalk.underline.cyan(slugline)} created ${chalk.green(
        "successfully"
      )}!`
    );
  }
}
