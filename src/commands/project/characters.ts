import { Command, flags } from "@oclif/command";
import { cli } from "cli-ux";
import api from "../../api";
import graphql from "../../graphql";
import { configparam, configstore } from "../../configstore";

export default class ProjectCharacters extends Command {
  static description = "List the characters for the active project.";

  static aliases = ["pr:ch"];

  static flags = {
    help: flags.help({ char: "h" }),
  };

  static args = [];

  async run() {
    let result = await api(graphql.query.charactersByProject, {
      projectId: configstore.get(configparam.PROJECT_ID),
    });
    const characters = result.data.data.charactersByProject;

    cli.table(
      characters,
      {
        number: {
          header: "#",
          minWidth: 3,
        },
        name: {
          minWidth: 7,
        },
        id: {
          header: "ID",
          extended: true,
        },
      },
      {
        printLine: this.log,
        ...flags, // parsed flags
      }
    );
  }
}
