import { Command, flags } from "@oclif/command";
import * as inquirer from "inquirer";
import api from "../../api";
import graphql from "../../graphql";
import { configparam, configstore } from "../../configstore";

export default class ProjectSet extends Command {
  static description = "Set the active team.";

  static aliases = ["pr:st"];

  static flags = {
    help: flags.help({ char: "h" }),
  };

  static args = [];

  async run() {
    let result = await api(graphql.query.projectsByTeam, {
      teamId: configstore.get(configparam.TEAM_ID),
    });
    const projects = result.data.data.projectsByTeam;

    if (!projects.length) {
      this.error("No projects exist for this team.");
    }

    const def = projects.find(
      (p: any) => p.id === configstore.get(configparam.PROJECT_ID)
    );

    const iq = await inquirer.prompt([
      {
        type: "list",
        name: "projectId",
        message: "Select project:",
        default: def?.id,
        choices: projects.map((project: any) => {
          return {
            name: project.title,
            value: project.id,
          };
        }),
      },
    ]);

    const { id, title } = projects.find((p: any) => p.id === iq.projectId);

    configstore.delete(configparam.PROJECT);
    configstore.set({ [configparam.PROJECT]: { id, title } });
  }
}
