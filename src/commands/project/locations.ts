import { Command, flags } from "@oclif/command";
import { cli } from "cli-ux";
import api from "../../api";
import graphql from "../../graphql";
import { configparam, configstore } from "../../configstore";

export default class ProjectLocations extends Command {
  static description = "List the locations for the active project.";

  static aliases = ["pr:lc"];

  static flags = {
    help: flags.help({ char: "h" }),
  };

  static args = [];

  async run() {
    let result = await api(graphql.query.locationsByProject, {
      projectId: configstore.get(configparam.PROJECT_ID),
    });
    const locations = result.data.data.locationsByProject;

    console.log(result.data);
    cli.table(
      locations,
      {
        name: {
          minWidth: 7,
        },
        id: {
          header: "ID",
          extended: true,
        },
      },
      {
        printLine: this.log,
        ...flags, // parsed flags
      }
    );
  }
}
