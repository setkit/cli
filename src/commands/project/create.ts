import { Command, flags } from "@oclif/command";
import chalk from "chalk";
import * as inquirer from "inquirer";
import api from "../../api";
import graphql from "../../graphql";
import { configparam, configstore } from "../../configstore";

export default class ProjectCreate extends Command {
  static description = "Create a new project within active team.";

  static aliases = ["pr:cr"];

  static flags = {
    help: flags.help({ char: "h" }),
    title: flags.string({
      char: "t",
      description: "Title of the new project.",
    }),
  };

  static args = [];

  async run() {
    const { flags } = this.parse(ProjectCreate);

    let projectTitle;
    if (flags.title) {
      projectTitle = flags.title;
    } else {
      const iq = await inquirer.prompt([
        {
          type: "input",
          name: "projectTitle",
          message: "Project title:",
        },
      ]);
      projectTitle = iq.projectTitle;
    }

    let result = await api(graphql.mutation.createProject, {
      title: projectTitle,
    });
    const { id, title } = result.data.data.createProject;

    configstore.delete(configparam.PROJECT);
    configstore.set({ [configparam.PROJECT]: { id, title } });
    console.log(
      `Project ${chalk.underline.cyan(title)} created ${chalk.green(
        "successfully"
      )}!`
    );
  }
}
