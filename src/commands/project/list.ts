import { Command, flags } from "@oclif/command";
import { cli } from "cli-ux";
import api from "../../api";
import graphql from "../../graphql";
import { configparam, configstore } from "../../configstore";

export default class ProjectList extends Command {
  static description = "Display the current team's project names.";

  static aliases = ["pr:ls"];

  static flags = {
    help: flags.help({ char: "h" }),
  };

  static args = [];

  async run() {
    let result = await api(graphql.query.projectsByTeam, {
      teamId: configstore.get(configparam.TEAM_ID),
    });
    const projects = result.data.data.projectsByTeam;

    if (!projects.length) {
      this.error("No projects exist for this team.");
    }

    cli.table(
      projects,
      {
        id: {
          header: "ID",
          extended: true,
        },
        title: {
          minWidth: 7,
        },
      },
      {
        printLine: this.log,
        ...flags, // parsed flags
      }
    );
  }
}
