import { Command, flags } from "@oclif/command";
import { configstore } from "../configstore";

export default class Logout extends Command {
  static description = "describe the command here";

  static flags = {
    help: flags.help({ char: "h" }),
  };

  static args = [];

  async run() {
    configstore.clear();

    console.log("Logout successful.");
  }
}
