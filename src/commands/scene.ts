import { Command, flags } from "@oclif/command";
import chalk from "chalk";
import { configparam, configstore } from "../configstore";

export default class Scene extends Command {
  static description = "describe the command here";

  static aliases = ["sc"];

  static flags = {
    help: flags.help({ char: "h" }),
  };

  static args = [];

  async run() {
    console.log(
      chalk.underline.cyan(configstore.get(configparam.SCENE_SLUGLINE))
    );
  }
}
