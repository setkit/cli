import { Command, flags } from "@oclif/command";
import * as inquirer from "inquirer";
import api from "../../api";
import graphql from "../../graphql";
import { configparam, configstore } from "../../configstore";

export default class CharacterLink extends Command {
  static description = "describe the command here";

  static aliases = ["ch:lk"];

  static flags = {
    help: flags.help({ char: "h" }),
  };

  static args = [];

  async run() {
    let result = await api(graphql.query.charactersByProject, {
      projectId: configstore.get(configparam.PROJECT_ID),
    });
    const characters = result.data.data.charactersByProject;

    const iq = await inquirer.prompt([
      {
        type: "list",
        name: "characterId",
        message: "Select character:",
        choices: characters.map((character: any) => {
          return {
            name: `${character.number}. ${character.name}`,
            value: character.id,
          };
        }),
      },
      {
        type: "confirm",
        name: "confirm",
        default: true,
        message: `Link to active scene: ${configstore.get(
          configparam.SCENE_SLUGLINE
        )}`,
      },
    ]);

    if (!iq.confirm) {
      console.log("Characters not linked.");
      return;
    }

    result = await api(graphql.mutation.linkCharacterToScene, {
      characterId: iq.characterId,
      sceneId: configstore.get(configparam.SCENE_ID),
    });

    const scene = result.data.data.linkCharacterToScene;
    console.log(scene.slugline);
    scene.characters.foreach((c: any) => {
      console.log(`${c.number}: ${c.name}`);
    });
  }
}
