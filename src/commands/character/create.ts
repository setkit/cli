import { Command, flags } from "@oclif/command";
import chalk from "chalk";
import * as inquirer from "inquirer";
import api from "../../api";
import graphql from "../../graphql";

export default class CharacterCreate extends Command {
  static description = "describe the command here";

  static aliases = ["ch:cr"];

  static flags = {
    help: flags.help({ char: "h" }),
    name: flags.string({ char: "n", description: "Name of new character." }),
  };

  static args = [];

  async run() {
    const { flags } = this.parse(CharacterCreate);

    let characterName;
    if (flags.name) {
      characterName = flags.name;
    } else {
      const iq = await inquirer.prompt([
        {
          type: "input",
          name: "characterName",
          message: "Character name:",
        },
      ]);
      characterName = iq.characterName;
    }

    let result = await api(graphql.mutation.createCharacter, {
      name: characterName,
    });
    const { name } = result.data.data.createCharacter;

    console.log(
      `Character ${chalk.underline.cyan(name)} created ${chalk.green(
        "successfully"
      )}!`
    );
  }
}
