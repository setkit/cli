import { Command, flags } from "@oclif/command";
import chalk from "chalk";
import { configparam, configstore } from "../configstore";

export default class Team extends Command {
  static description = "Display the active team name.";

  static aliases = ["tm"];

  static flags = {
    help: flags.help({ char: "h" }),
  };

  static args = [];

  async run() {
    console.log(chalk.underline.cyan(configstore.get(configparam.TEAM_NAME)));
  }
}
