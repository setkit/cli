import { Command, flags } from "@oclif/command";
import chalk from "chalk";
import { configparam, configstore } from "../configstore";

export default class Project extends Command {
  static description = "Display the active project title.";

  static aliases = ["pr"];

  static flags = {
    help: flags.help({ char: "h" }),
  };

  static args = [];

  async run() {
    console.log(
      chalk.underline.cyan(configstore.get(configparam.PROJECT_TITLE))
    );
  }
}
