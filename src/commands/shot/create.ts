import { Command, flags } from "@oclif/command";
import chalk from "chalk";
import * as inquirer from "inquirer";
import api from "../../api";
import graphql from "../../graphql";
import { configparam, configstore } from "../../configstore";

export default class ShotCreate extends Command {
  static description = "describe the command here";

  static aliases = ["sh:cr"];

  static flags = {
    help: flags.help({ char: "h" }),
    angle: flags.string({ char: "a", description: "name to print" }),
    description: flags.string({ char: "d", description: "name to print" }),
    grip: flags.string({ char: "g", description: "name to print" }),
    movement: flags.string({ char: "m", description: "name to print" }),
    type: flags.string({ char: "t", description: "name to print" }),
  };

  static args = [];

  async run() {
    const { flags } = this.parse(ShotCreate);
    let shotAngle, shotDescription, shotGrip, shotMovement, shotType;

    let choices = ["DEGREE_45", "FRONT", "HIGH", "LOW", "PROFILE", "SIDE"];
    if (flags.angle) {
      shotAngle = flags.angle.toUpperCase();

      if (!choices.includes(shotAngle)) this.error("Invalid shot angle.");
    } else {
      const iq = await inquirer.prompt([
        {
          type: "list",
          name: "shotAngle",
          message: "Select the shot angle:",
          choices: choices,
        },
      ]);
      shotAngle = iq.shotAngle;
    }

    if (flags.description) {
      shotDescription = flags.description;
    } else {
      const iq = await inquirer.prompt([
        {
          type: "input",
          name: "shotDescription",
          message: "Shot decription:",
        },
      ]);
      shotDescription = iq.shotDescription;
    }

    let result = await api(graphql.mutation.createShot, {
      angle: shotAngle,
      description: shotDescription,
    });
    const { id } = result.data.data.createShot;

    console.log(`Scene created ${chalk.green("successfully")}!`);
  }
}
