export const messages = {
  PROJECT_NOT_SET: "Select or create a project.",
  SCENE_NOT_SET: "Select or create a scene.",
  TEAM_NOT_SET: "Select or create a team.",
};
