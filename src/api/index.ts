import axios from "axios";
import { DocumentNode } from "graphql";
import { exit } from "process";
require("dotenv").config();

let url = "https://graphql.setkit.app";

const instance = axios.create({
  url,
  headers: { Authorization: `Bearer ${process.env.BEARER_TOKEN}` },
});

const api = async (query: DocumentNode, variables?: any) => {
  let result;
  try {
    result = await instance({
      method: "POST",
      data: {
        query,
        variables,
      },
    });
  } catch (error) {
    console.log(error.message);
    exit();
  }

  if (result?.data.errors) {
    console.log(result.data.errors[0].message);
    exit();
  }

  return result;
};

export default api;
