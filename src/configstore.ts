import Configstore from "configstore";

// import * as logger from "./logger";

// eslint-disable-next-line @typescript-eslint/no-var-requires
const pkg = require("../package.json");

export const configstore = new Configstore(pkg.name);

export const configparam = {
  // Project
  PROJECT: "team.project",
  PROJECT_TITLE: "team.project.title",
  PROJECT_ID: "team.project.id",
  // Scene
  SCENE: "team.project.scene",
  SCENE_ID: "team.project.scene.id",
  SCENE_SLUGLINE: "team.project.scene.slugline",
  // Team
  TEAM: "team",
  TEAM_NAME: "team.name",
  TEAM_ID: "team.id",
};
