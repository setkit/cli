# @setkit/cli

CLI tool for interacting with Setkit.

[![oclif](https://img.shields.io/badge/cli-oclif-brightgreen.svg)](https://oclif.io)
[![Version](https://img.shields.io/npm/v/@setkit/cli.svg)](https://npmjs.org/package/@setkit/cli)
[![Downloads/week](https://img.shields.io/npm/dw/@setkit/cli.svg)](https://npmjs.org/package/@setkit/cli)
[![License](https://img.shields.io/npm/l/@setkit/cli.svg)](https://gitlab.com/setkit/cli/blob/master/package.json)

<!-- toc -->
* [@setkit/cli](#setkitcli)
* [Usage](#usage)
* [Commands](#commands)
<!-- tocstop -->

# Usage

<!-- usage -->
```sh-session
$ npm install -g @setkit/cli
$ setkit COMMAND
running command...
$ setkit (-v|--version|version)
@setkit/cli/0.1.4 darwin-x64 node-v13.10.1
$ setkit --help [COMMAND]
USAGE
  $ setkit COMMAND
...
```
<!-- usagestop -->

# Commands

<!-- commands -->
* [`setkit character:create`](#setkit-charactercreate)
* [`setkit character:link`](#setkit-characterlink)
* [`setkit commands`](#setkit-commands)
* [`setkit help [COMMAND]`](#setkit-help-command)
* [`setkit location:create [FILE]`](#setkit-locationcreate-file)
* [`setkit login`](#setkit-login)
* [`setkit logout`](#setkit-logout)
* [`setkit project`](#setkit-project)
* [`setkit project:characters`](#setkit-projectcharacters)
* [`setkit project:create`](#setkit-projectcreate)
* [`setkit project:list`](#setkit-projectlist)
* [`setkit project:locations [FILE]`](#setkit-projectlocations-file)
* [`setkit project:set`](#setkit-projectset)
* [`setkit scene`](#setkit-scene)
* [`setkit scene:characters`](#setkit-scenecharacters)
* [`setkit scene:create`](#setkit-scenecreate)
* [`setkit scene:list`](#setkit-scenelist)
* [`setkit scene:set`](#setkit-sceneset)
* [`setkit scene:shots [FILE]`](#setkit-sceneshots-file)
* [`setkit shot:create`](#setkit-shotcreate)
* [`setkit team`](#setkit-team)
* [`setkit team:create`](#setkit-teamcreate)
* [`setkit team:list`](#setkit-teamlist)
* [`setkit team:set`](#setkit-teamset)

## `setkit character:create`

describe the command here

```
USAGE
  $ setkit character:create

OPTIONS
  -h, --help       show CLI help
  -n, --name=name  Name of new character.

ALIASES
  $ setkit ch:cr
```

_See code: [src/commands/character/create.ts](https://gitlab.com/setkit/cli/blob/v0.1.4/src/commands/character/create.ts)_

## `setkit character:link`

describe the command here

```
USAGE
  $ setkit character:link

OPTIONS
  -h, --help  show CLI help

ALIASES
  $ setkit ch:lk
```

_See code: [src/commands/character/link.ts](https://gitlab.com/setkit/cli/blob/v0.1.4/src/commands/character/link.ts)_

## `setkit commands`

list all the commands

```
USAGE
  $ setkit commands

OPTIONS
  -h, --help              show CLI help
  -j, --json              display unfiltered api data in json format
  -x, --extended          show extra columns
  --columns=columns       only show provided columns (comma-separated)
  --csv                   output is csv format [alias: --output=csv]
  --filter=filter         filter property by partial string matching, ex: name=foo
  --hidden                show hidden commands
  --no-header             hide table header from output
  --no-truncate           do not truncate output to fit screen
  --output=csv|json|yaml  output in a more machine friendly format
  --sort=sort             property to sort by (prepend '-' for descending)
```

_See code: [@oclif/plugin-commands](https://github.com/oclif/plugin-commands/blob/v1.3.0/src/commands/commands.ts)_

## `setkit help [COMMAND]`

display help for setkit

```
USAGE
  $ setkit help [COMMAND]

ARGUMENTS
  COMMAND  command to show help for

OPTIONS
  --all  see all commands in CLI
```

_See code: [@oclif/plugin-help](https://github.com/oclif/plugin-help/blob/v3.1.0/src/commands/help.ts)_

## `setkit location:create [FILE]`

describe the command here

```
USAGE
  $ setkit location:create [FILE]

OPTIONS
  -h, --help       show CLI help
  -n, --name=name  Name of new location.

ALIASES
  $ setkit lc:cr
```

_See code: [src/commands/location/create.ts](https://gitlab.com/setkit/cli/blob/v0.1.4/src/commands/location/create.ts)_

## `setkit login`

describe the command here

```
USAGE
  $ setkit login

OPTIONS
  -h, --help       show CLI help
  -n, --name=name  name to print
```

_See code: [src/commands/login.ts](https://gitlab.com/setkit/cli/blob/v0.1.4/src/commands/login.ts)_

## `setkit logout`

describe the command here

```
USAGE
  $ setkit logout

OPTIONS
  -h, --help  show CLI help
```

_See code: [src/commands/logout.ts](https://gitlab.com/setkit/cli/blob/v0.1.4/src/commands/logout.ts)_

## `setkit project`

Display the active project title.

```
USAGE
  $ setkit project

OPTIONS
  -h, --help  show CLI help

ALIASES
  $ setkit pr
```

_See code: [src/commands/project.ts](https://gitlab.com/setkit/cli/blob/v0.1.4/src/commands/project.ts)_

## `setkit project:characters`

List the characters for the active project.

```
USAGE
  $ setkit project:characters

OPTIONS
  -h, --help  show CLI help

ALIASES
  $ setkit pr:ch
```

_See code: [src/commands/project/characters.ts](https://gitlab.com/setkit/cli/blob/v0.1.4/src/commands/project/characters.ts)_

## `setkit project:create`

Create a new project within active team.

```
USAGE
  $ setkit project:create

OPTIONS
  -h, --help         show CLI help
  -t, --title=title  Title of the new project.

ALIASES
  $ setkit pr:cr
```

_See code: [src/commands/project/create.ts](https://gitlab.com/setkit/cli/blob/v0.1.4/src/commands/project/create.ts)_

## `setkit project:list`

Display the current team's project names.

```
USAGE
  $ setkit project:list

OPTIONS
  -h, --help  show CLI help

ALIASES
  $ setkit pr:ls
```

_See code: [src/commands/project/list.ts](https://gitlab.com/setkit/cli/blob/v0.1.4/src/commands/project/list.ts)_

## `setkit project:locations [FILE]`

describe the command here

```
USAGE
  $ setkit project:locations [FILE]

OPTIONS
  -f, --force
  -h, --help       show CLI help
  -n, --name=name  name to print
```

_See code: [src/commands/project/locations.ts](https://gitlab.com/setkit/cli/blob/v0.1.4/src/commands/project/locations.ts)_

## `setkit project:set`

Set the active team.

```
USAGE
  $ setkit project:set

OPTIONS
  -h, --help  show CLI help

ALIASES
  $ setkit pr:st
```

_See code: [src/commands/project/set.ts](https://gitlab.com/setkit/cli/blob/v0.1.4/src/commands/project/set.ts)_

## `setkit scene`

describe the command here

```
USAGE
  $ setkit scene

OPTIONS
  -h, --help  show CLI help

ALIASES
  $ setkit sc
```

_See code: [src/commands/scene.ts](https://gitlab.com/setkit/cli/blob/v0.1.4/src/commands/scene.ts)_

## `setkit scene:characters`

describe the command here

```
USAGE
  $ setkit scene:characters

OPTIONS
  -h, --help  show CLI help

ALIASES
  $ setkit sc:ch
```

_See code: [src/commands/scene/characters.ts](https://gitlab.com/setkit/cli/blob/v0.1.4/src/commands/scene/characters.ts)_

## `setkit scene:create`

describe the command here

```
USAGE
  $ setkit scene:create

OPTIONS
  -h, --help               show CLI help
  -l, --location=location  Location of new scene
  -s, --setting=setting    Setting of new scene.
  -t, --tod=tod            Time of day of the scene

ALIASES
  $ setkit sc:cr
```

_See code: [src/commands/scene/create.ts](https://gitlab.com/setkit/cli/blob/v0.1.4/src/commands/scene/create.ts)_

## `setkit scene:list`

describe the command here

```
USAGE
  $ setkit scene:list

OPTIONS
  -h, --help  show CLI help

ALIASES
  $ setkit sc:ls
```

_See code: [src/commands/scene/list.ts](https://gitlab.com/setkit/cli/blob/v0.1.4/src/commands/scene/list.ts)_

## `setkit scene:set`

describe the command here

```
USAGE
  $ setkit scene:set

OPTIONS
  -h, --help  show CLI help

ALIASES
  $ setkit sc:st
```

_See code: [src/commands/scene/set.ts](https://gitlab.com/setkit/cli/blob/v0.1.4/src/commands/scene/set.ts)_

## `setkit scene:shots [FILE]`

describe the command here

```
USAGE
  $ setkit scene:shots [FILE]

OPTIONS
  -h, --help  show CLI help

ALIASES
  $ setkit sc:sh
```

_See code: [src/commands/scene/shots.ts](https://gitlab.com/setkit/cli/blob/v0.1.4/src/commands/scene/shots.ts)_

## `setkit shot:create`

describe the command here

```
USAGE
  $ setkit shot:create

OPTIONS
  -a, --angle=angle              name to print
  -d, --description=description  name to print
  -g, --grip=grip                name to print
  -h, --help                     show CLI help
  -m, --movement=movement        name to print
  -t, --type=type                name to print

ALIASES
  $ setkit sh:cr
```

_See code: [src/commands/shot/create.ts](https://gitlab.com/setkit/cli/blob/v0.1.4/src/commands/shot/create.ts)_

## `setkit team`

Display the active team name.

```
USAGE
  $ setkit team

OPTIONS
  -h, --help  show CLI help

ALIASES
  $ setkit tm
```

_See code: [src/commands/team.ts](https://gitlab.com/setkit/cli/blob/v0.1.4/src/commands/team.ts)_

## `setkit team:create`

Create a new team.

```
USAGE
  $ setkit team:create

OPTIONS
  -h, --help       show CLI help
  -n, --name=name  Name of new team.

ALIASES
  $ setkit tm:cr
```

_See code: [src/commands/team/create.ts](https://gitlab.com/setkit/cli/blob/v0.1.4/src/commands/team/create.ts)_

## `setkit team:list`

Display the current user's team names.

```
USAGE
  $ setkit team:list

OPTIONS
  -h, --help  show CLI help

ALIASES
  $ setkit tm:ls
```

_See code: [src/commands/team/list.ts](https://gitlab.com/setkit/cli/blob/v0.1.4/src/commands/team/list.ts)_

## `setkit team:set`

Set the active team.

```
USAGE
  $ setkit team:set

OPTIONS
  -h, --help  show CLI help

ALIASES
  $ setkit tm:st
```

_See code: [src/commands/team/set.ts](https://gitlab.com/setkit/cli/blob/v0.1.4/src/commands/team/set.ts)_
<!-- commandsstop -->
