import {expect, test} from '@oclif/test'

describe('scene:characters', () => {
  test
  .stdout()
  .command(['scene:characters'])
  .it('runs hello', ctx => {
    expect(ctx.stdout).to.contain('hello world')
  })

  test
  .stdout()
  .command(['scene:characters', '--name', 'jeff'])
  .it('runs hello --name jeff', ctx => {
    expect(ctx.stdout).to.contain('hello jeff')
  })
})
