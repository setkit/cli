import {expect, test} from '@oclif/test'

describe('scene:shots', () => {
  test
  .stdout()
  .command(['scene:shots'])
  .it('runs hello', ctx => {
    expect(ctx.stdout).to.contain('hello world')
  })

  test
  .stdout()
  .command(['scene:shots', '--name', 'jeff'])
  .it('runs hello --name jeff', ctx => {
    expect(ctx.stdout).to.contain('hello jeff')
  })
})
