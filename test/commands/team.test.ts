import { expect, test } from "@oclif/test";
import { configparam, configstore } from "../../src/configstore";
import { messages } from "../../src/errors";

describe("team", () => {
  test
    .stdout()
    .command(["team"])
    .it("runs team", (ctx) => {
      const teamName = configstore.get(configparam.TEAM_NAME);

      if (!teamName) {
        expect(ctx.stdout).throws(messages.TEAM_NOT_SET);
      }

      expect(ctx.stdout).to.contain(teamName);
    });
});
